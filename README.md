
## Advanced Statistical Computing - 2021 Fall

**Name:** Xinlei Zhang

**Email Address:** Xinleiz@vt.edu

This repository is for storing class materials of STAT 6554: Advanced Statistical Computing and submitting homeworks.

The homework directory contains the homework solutions and the note directory contains important class notes. 
