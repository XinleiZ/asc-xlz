#!/bin/bash
#SBATCH -t 30:00:00 
#SBATCH -p normal_q 
#SBATCH -A ascclass
#SBATCH -N5 --ntasks-per-node=16



### Add modules
rver="3.6.2" #R version
module purge
module load intel/18.2 mkl R/$rver openmpi/4.0.1 R-parallel/$rver

#set r libraries
export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

# Set num threads
export MKL_NUM_THREADS=8

# OpenMPI environment variables
export OMPI_MCA_btl_openib_allow_ib=1 #allow infiniband
export OMPI_MCA_rmaps_base_inherit=1  #slaves inherit environment

#nodes assigned to job
scontrol show hostname $SLURM_NODELIST > nodes.list

# Run R
# --map-by creates a process on each node
# --bind-to lets OpenMP run across the allocated cores on the node
SCRIPT=spam_snow.R 
#mpirun -np 1 Rscript $SCRIPT & #bad mapping
#mpirun -np 1 --map-by ppr:1:node Rscript $SCRIPT & #good mapping, but stuck in 1-core box
mpirun -np 1 --hostfile nodes.list --map-by ppr:2:node --bind-to none Rscript $SCRIPT & 
#ppr:2:node, 2 = ntasks-per-node/number of thread

## Solution to the MPI busy-wait problem
while [ 1 ]; do
    sleep 1
    PID=$(pgrep -f "R --slave --no-restore --file=$SCRIPT")
    [ -z "$PID" ] || break
done

renice 19 $PID

wait

#exit;
