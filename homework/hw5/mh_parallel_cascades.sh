#!/bin/bash

# Sample submission script for parallel Metropolis-Hastings

#SBATCH -t 00:30:00
#SBATCH -N 2 --ntasks-per-node=5
#SBATCH -p dev_q

# Add modules
module purge
module load intel/18.2 openmpi/4.0.1 R/3.6.1 R-parallel/3.6.1

#set r libraries
export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

# Run R
# Rscript mh_parallel.r

# --oversubscribe so we can start as many child processes as cores
mpirun -np 1 --oversubscribe Rscript mh_parallel.r
