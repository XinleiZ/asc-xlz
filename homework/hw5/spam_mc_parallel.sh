#!/bin/bash

#SBATCH -N 5
#SBATCH --ntasks-per-node=32
#SBATCH -t 30:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module purge
module load parallel
module load intel/18.2 mkl R/3.6.2

#set r libraries
export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

#set number of cores used by each r process
export MKL_NUM_THREADS=8

#number of r processes to run
ncopies=16   #ncopies * MKL_NUM_THREADS = nnodes * ntasks-per-node

#processes to run at a time
nparallel=4

#nodes assigned to job
scontrol show hostname $SLURM_NODELIST > node.list

echo "$( date ): Starting spam_mc"

# for i in $( seq 1 $ncopies ); do 
#   R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout &
# done
# wait
seq 1 $ncopies | parallel -j$nparallel --sshloginfile node.list --workdir $SLURM_SUBMIT_DIR --env R_LIBS --env MKL_NUM_THREADS "module load intel/18.2 mkl R/3.6.2; export MKL_NUM_THREADS=8; R CMD BATCH \"--args seed={} reps=2\" spam_mc.R spam_mc_{}.Rout"

echo "$( date ): Finished spam_mc"


echo "$( date ): Starting spam_mc_collect"
R CMD BATCH spam_mc_collect.R spam_mc_collect.Rout
echo "$( date ): Finished spam_mc_collect"

