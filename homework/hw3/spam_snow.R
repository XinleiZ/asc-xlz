## 
## begin command line processing
##

## set the seed
seed <- 0
## specify the number of repetitions
reps <- 5
## specify the number of CV folds
folds <- 5
## specify the the number of parallel instances to create:
np <- 4

## read in the command line arguments
## run with: R CMD BATCH "--args reps=5 folds=5 np=4" spam_snow.R
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text=args[[i]]))

## print seed
cat("seed is ", seed, "\n", sep="")
set.seed(seed)
cat("reps is ", reps, "\n", sep="")
cat("folds is ", folds, "\n", sep="")
cat("number of sockets is ",np, "\n", sep="")


## 
## begin MC analysis 
##

## get the data
spam <- read.csv("spam.csv")

#MC_bakeoff handles chunks of repetitions;
#number of CV folds, and number of repetitions can be
#specified by the user.
MC_bakeoff <- function(rchunks,spam,folds,reps){
## load libraries
library(rpart)
library(mda)
library(MASS)
library(randomForest)

## function for creating CV folds
cv.folds <- function (n, folds = 10)
  split(sample(1:n), rep(1:folds, length = n))

## allocate space for the predictor
pnull <- matrix(NA, nrow=reps, ncol=nrow(spam))
pfull <- pfwd <- pfwdi <- plda <- pqda <- pnull
pfda <- prp <- pmnlm <- prf <- pnull

## loop over repetitions
for(r in rchunks) {
  
  ## generate CV folds
  all.folds <- cv.folds(nrow(spam), folds) 
  
  ## loop over folds
  for(i in 1:length(all.folds)) {
    
    ## get the ith fold, and print progress
    o <- all.folds[[i]]
    cat("(r,i)=(", r, ",", i, ")\n", sep="")
    
    ## training and testing set
    test <- spam[o,]
    train <- spam[-o,]
    
    ## get rid of colums that are all the same
    one <- rep(1, nrow(train))
    zo <- which(sqrt(drop(one %*% (as.matrix(train)^2))) == 0)
    if(length(zo) > 0) {
      train <- train[,-zo]
      test <- test[,-zo]
    }
    nt <- nrow(train)
    
    ## defining the universe of variables
    null <- glm(spam ~ 1, data=train, family="binomial")
    pnull[r,o] <- round(predict(null, newdata=test, type="response"))
    
    ## the full glm
    full <- suppressWarnings(glm(spam ~ ., data=train, 
                                 family="binomial")) ## will warn
    pfull[r,o] <- round(predict(full, newdata=test, type="response"))
    
    ## go forward to get the best model greedily
    fwd <- suppressWarnings(step(null, scope=formula(full), 
                                 k=log(nt), trace=0))
    pfwd[r,o] <- round(predict(fwd, newdata=test, type="response"))
    
    ## now try adding in interactions
    fwdi <- suppressWarnings(step(fwd, scope=~.+.^2, k=log(nt), trace=0))
    ## this takes a long time (about an hour on a fast machine)
    pfwdi[r,o] <- round(predict(fwdi, newdata=test, type="response"))
    
    ## LDA
    spam.lda <- lda(spam~., data=train)
    plda[r,o] <- as.numeric(predict(spam.lda, newdata=test)$class)-1
    
    ## QDA
    try({  ## necessary because quadratic expansion may not work
      spam.qda <- qda(spam~., data=train);
      pqda[r,o] <- as.numeric(predict(spam.qda, newdata=test)$class)-1
    }, silent=TRUE)
    
    ## FDA
    spam.fda <- fda(spam~., data=train)
    pfda[r,o] <- as.numeric(predict(spam.fda, newdata=test, type="class"))-1
    
    ## rpart
    ## otherwise rpart will do regression
    train <- transform(train, spam=as.factor(spam))
    spam.rp <- rpart(spam~., data=train)
    prp[r,o] <- as.numeric(predict(spam.rp, newdata=test, type="class"))-1
    
    ## random forests
    rf <- randomForest(spam~., data=train)
    prf[r,o] <- as.numeric(predict(rf, newdata=test))-1
    
  }
}
 return(list("pnull"=pnull[rchunks,],"pfull"=pfull[rchunks,],"pfwd"=pfwd[rchunks,],"pfwdi"=pfwdi[rchunks,],"plda"=plda[rchunks,],
            "pqda"=pqda[rchunks,],"pfda"=pfda[rchunks,],"prp"=prp[rchunks,],"pmnlm"=pmnlm[rchunks,],"prf"=prf[rchunks,]))
}

## run on np processors
library(parallel)
cl <- makeCluster(type="PSOCK", rep("localhost", np))
suppressWarnings(rchunks <- split(1:reps, 1:np))
res <- clusterApply(cl,rchunks,MC_bakeoff,spam,folds,reps)
stopCluster(cl)

##combine outputs of chunks of reps

## empty initialization
pfull <- pfwd <- pfwdi <- plda <- pqda <- pnull <- NULL
pfda <- prp <- pmnlm <- prf <- pnull 

for(i in 1:np){
  pnull <- rbind(pnull, res[[i]]$pnull); pfull <- rbind(pfull, res[[i]]$pfull); pfwd <- rbind(pfwd, res[[i]]$pfwd) 
  pfwdi <- rbind(pfwdi, res[[i]]$pfwdi); plda <- rbind(plda, res[[i]]$plda); pqda <- rbind(pqda, res[[i]]$pqda)
  pfda <- rbind(pfda, res[[i]]$pfda); prp <- rbind(prp, res[[i]]$prp);pmnlm <- rbind(pmnlm, res[[i]]$pmnlm)
  prf <- rbind(prf, res[[i]]$prf)
}

## save results to a file
save(pnull, pfull, pfwd, pfwdi, plda, pqda, pfda, prp, prf, 
     file=paste("spam_para_", seed, ".RData", sep=""))

