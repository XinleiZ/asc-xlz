#!/bin/bash

np=${1}
cores=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu || echo "$NUMBER_OF_PROCESSORS")

if [ -z "$np" ]
then
      np=$cores
else
if (("$np" > "$cores"))
then
 echo Warning: The argument is greater than the number of cores:$cores on the machine. 
 np=$cores
fi
fi
echo Number of parallel instances:$np

echo "$( date ): Starting spam_mc"

for ((i = 1; i <= np; i++))
do
  nohup R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout&
done
wait

echo "$( date ): Finished spam_mc"

