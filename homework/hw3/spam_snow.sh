#!/bin/bash

cores=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu || echo "$NUMBER_OF_PROCESSORS")

echo Please specify the the number of parallel instances to create \(default = 4\):
read np

echo Please specify the number of CV folds:
read folds

echo Please specify the number of CV repetitions:
read reps


if [ -z "$np" ]
then  
    np=4
fi

if (("$np" > "$cores"))
then
 echo Warning: The argument is greater than the number of cores:$cores on the machine. 
 np=$cores
fi

echo Number of parallel instances to create:$np

echo "$( date ): Starting spam_snow"

nohup R CMD BATCH "--args reps=$reps folds=$folds np=$np" spam_snow.R spam_snow.Rout&
wait

echo "$( date ): Finished spam_snow"

