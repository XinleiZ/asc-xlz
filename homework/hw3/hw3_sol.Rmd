---
title: "Homework 3"
subtitle: "Advanced Statistical Computing (STAT 6554)"
author: "Xinlei Zhang (<xinleiz@vt.edu>) <br> Department of Statistics, Virginia Tech"
output: html_document
---

```{r setup, include=FALSE}
library(knitr)
knit_hooks$set(no.main = function(before, options, envir) {
    if (before) par(mar = c(4.1, 4.1, 1.1, 1.1))  # smaller margin on top
})
## ```{r, dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
```

## Problem 1: Profiling (20 pts)

Below are functions from the lecture:

```{r}
## slow code for computing powers of a matrix
##
## powers1: forms a matrix of powers of the
## vector x up to the degree dg

powers1 <- function(x, dg)
  {
    pw <- matrix(x, nrow=length(x))
    prod <- x ## current product
    for(i in 2:dg) {
      prod <- prod * x
      pw <- cbind(pw, prod)
    }
    return(pw)
  }
## a faster powers function
##
## powers2: same as above but with pre-allocation

powers2 <- function(x, dg)
  {
    pw <- matrix(nrow=length(x), ncol=dg)
    prod <- x ## current product
    pw[,1] <- prod
    for(i in 2:dg) {
      prod <- prod * x
      pw[,i] <- prod
    }
  }

```


Below are two, very compact, versions of functions for calculating powers of a matrix.  

```{r}
powers3 <- function(x, dg) outer(x, 1:dg, "^")
powers4 <- function(x, dg) 
 {
   repx <- matrix(rep(x, dg), nrow=length(x))
   return(t(apply(repx, 1, cumprod)))
 }
```

a.  First, briefly explain the thought process behind these two new methods.

For powers3, the function forms a matrix of powers of the vector x up to the degree dg by performing an outer product of the vector x and vector y ($[1,2,..,dg]$) with the power function ("^"). As a result, the $ij_{th}$ element in the resulting matrix is $x_i^{y_j}.$

For powers4, the function firstly creates a matrix, "repx", by replicating the vector x dg times and setting the number of rows of the matrix as the length of the vector x. With the apply function, it calculates the cumulative products for each row of the matrix, repx. Finally, the function returns the transpose of the resulting matrix returned by the apply function.

b.  Then provide a summary of the computation time for all four versions (two from lecture and the two above).  Use the same `x` from lecture.

```{r}
x <- runif(10000000)
t1<-system.time(p1 <- powers1(x, 16))[3]
t2<-system.time(p2 <- powers2(x, 16))[3]
t3<-system.time(p3 <- powers3(x, 16))[3]
t4<-system.time(p4 <- powers4(x, 16))[3]
```

```{r}
time <- data.frame(t(c(t1,t2,t3,t4)))
colnames(time) <- c("powers1","powers2","powers3","powers4")
rownames(time) <- c("Elapsed time(s)")
knitr::kable(time)
```

From the table above, powers4 has the longest running time, while powers2 has the shortest running time. The running times for powers1 and powers3 are similar. 

c.  Profile the code to explain why the two new versions disappoint relative to the original two.  Cite particular subroutines which cause the slowdowns with reference to the profile summaries.  Are these subroutines they creating memory or computational bottlenecks, or both?

```{r}
Rprof(memory.profiling = T)
p3 <- powers3(x, 16)
Rprof(NULL)
summaryRprof(memory = "both") -> s3
```

```{r}
s3$by.self[1,] #by.self
s3$by.total[1,] #by.total
```

From the profile summaries, function, outer, appears at the top of both lists. Thus, the outer function is the cause for most concern. All the computation time is spent on the outer function, but the memory took by the outer function is not very large (~2442.1 bytes). Thus, the outer function creates computational bottlenecks.

```{r}
Rprof(memory.profiling = T)
p4 <- powers4(x, 16)
Rprof(NULL)
summaryRprof(memory = "both") -> s4
```

```{r}
s4$by.self[c(1,2,3,4,5),] #by.self
bytotal<-s4$by.total
#find index b/c output changes when knit to html
index<-(rownames(bytotal) ==  "\"apply\""  | rownames(bytotal) == "\"t.default\""  | rownames(bytotal) == "\"unlist\"")
bytotal[index,] #by.total
```

From the profile summaries, function apply and t.default appear near the top of both lists. The apply requires a lot of computation effort and memory. Comparing to the apply function, the required computation time and memory for t.default function are smaller. Thus, the apply function is the cause for most concern, and it creates both memory and computational bottlenecks. In addition, t.default also creates computational bottlenecks. 


## Problem 2: Annie & Sam (15 pts)

How would adjust the code for the Annie \& Sam example(s) to accommodate other distributions?  E.g., if $S \sim \mathcal{N}(10.5, 1)$ and $A\sim\mathcal{N}(11, 1.5)$?

The code can be adjusted by sampling Annie \& Sam's data from the desired distribution. For example, if $S \sim \mathcal{N}(10.5, 1)$ and $A\sim\mathcal{N}(11, 1.5)$, the adjusted code below will sample $S$ from $\mathcal{N}(10.5, 1)$ and sample $A$ from $\mathcal{N}(11, 1.5)$.

```{r}
#ASmc function adjust the code for the Annie & Sam example(s) to 
#accommodate unif and normal distributions, dist specify the 
#desired distribution, para_A contains an array of parameters 
#of the distribution for Annie, and para_S contains an array of parameters 
#of the distribution Sam. For unif, para_A,para_S=c(min,max). For normal, 
#para_A,para_S=c(mean,variance)

ASmc <- function(dist=c("normal","unif"),para_A,para_S){
  n <- 1000
  dist <- match.arg(dist)
  if(dist == "unif"){
    S <- runif(n,para_S[1],para_S[2])
    A <- runif(n,para_A[1],para_A[2])
  }
  else if(dist == "normal"){
    S <- rnorm(n,para_S[1],sqrt(para_S[2]))
    A <- rnorm(n,para_A[1],sqrt(para_A[2]))
  }
  
  prob <- mean(A < S) 
  se_prob <- sqrt(prob*(1-prob)/n)
  prob_CI <- prob+ c(-1,1)*1.96*se_prob
  
  diff <- A - S
  d <- c(mean(diff), mean(abs(diff))) 
  se_d <- c(sd(diff), sd(abs(diff)))/sqrt(n) 
  d1_CI <- d[1]+ c(-1,1)*1.96*se_d[1]
  d2_CI <- d[2]+ c(-1,1)*1.96*se_d[2]
  
  #print out results
  cat("The probability that Annie arrives before Sam:", prob,"\n")
  cat("The 95% CI of the probability is: (", prob_CI[1], ",", prob_CI[2],")\n", sep = "")
  cat("The expected difference in the two arrival times E{A-S}:", d[1],"\n")
  cat("The 95% CI of the expected difference is: (", d1_CI[1], ",", d2_CI[2],")\n", sep = "")
  cat("The expected absolute difference in the two arrival times E{|A-S|}:", d[2],"\n")
  cat("The 95% cCI of the expected absolute difference is: (", d2_CI[1], ",", d2_CI[2],")\n", sep = "")
  
}
```

```{r}
#example A follows N(mean=11,var=1.5) & S follows N(mean=10.5,var=1)
ASmc("normal",c(11,1.5),c(10.5,1))
```


## Problem 3: Bootstrap with `boot` (15 pts)

Re-write the least-squares regression bootstrap from lecture using the `boot` library. 

- Briefly compare and contrast to the results we obtained in lecture.

Code below performs the least-squares regression bootstrap using the `boot` library. 

```{r}
#boot function method
library(boot)
n <- 200 
X <- 1/cbind(rt(n, df=1), rt(n, df=1), rt(n,df=1)) 
beta <- c(1,2,3,0) 
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3) 
reg <- data.frame("X1"=X[,1],"X2"=X[,2],"X3"=X[,3],"Y"=Y)
reg_coef <- function(data, indices){
  coef(lm(Y ~., data = reg, subset = indices))
}
system.time(res<-boot(reg, reg_coef, 10000))
res
```


```{r}
#for loop method
system.time({
B <- 10000 
beta.hat.boot <- matrix(NA, nrow=B, ncol=length(beta)) 
for(b in 1:B) { 
    indices <- sample(1:nrow(X), nrow(X), replace=TRUE) 
    beta.hat.boot[b,] <- coef(lm(Y[indices]~X[indices,]))}
})
```

```{r}
#compare coefs
res
mean<-colMeans(beta.hat.boot) #mean
sd<-apply(beta.hat.boot,2,sd) #sd
cbind(mean,sd)
```

With B = 10000 bootstrap samples, the estimated coefficients via boot function are very similar to the average values of estimated coefficients obtained by the for loop method. Also, the standard errors of coefficients provided by the boot function are similar to the standard errors of estimated coefficients obtained by the for loop method. However, based on the outputs from the system.time function. The running time of the for loop method is actually shorter than the running time of the boot function with the same number of bootstrap samples on my laptop. 

We can also compare the marginals of the sampling distributions. From the graphs below, the boot function and the for loop method generate similar marginals of the sampling distributions with the same number of bootstrap samples

```{r, dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
## comparing marginals
fit <- lm(Y~X)
par(mfrow=c(2,2))
for(i in 1:4) {
  m <- coef(fit)[i]
  s <- sqrt(cov(beta.hat.boot)[i,i])
  x <- m + seq(-4*s, 4*s, length=1000)
  xlim <- range(c(x, beta.hat.boot[,i]))
  hist(beta.hat.boot[,i], freq=FALSE, xlim=xlim,
       xlab=i, main="", breaks=20)
  lines(x, dnorm(x, m, s), col=2, lwd=2)
}
beta.hat.boot2 <- res$t
par(mfrow=c(2,2))
for(i in 1:4) {
  m <- coef(fit)[i]
  s <- sqrt(cov(beta.hat.boot2)[i,i])
  x <- m + seq(-4*s, 4*s, length=1000)
  xlim <- range(c(x, beta.hat.boot2[,i]))
  hist(beta.hat.boot2[,i], freq=FALSE, xlim=xlim,
       xlab=i, main="", breaks=20)
  lines(x, dnorm(x, m, s), col=2, lwd=2)
}
```

