#!/bin/sh

filename="$1"

while read -r line; do
      s="$line"
      if [[ ${#s} -ge 12 && "$s" == *[a-z]* && "$s" == *[A-Z]* && "$s" == *[0-9]* && "$s" == *[\!@#$%^\&]*  && "$s" == [a-zA-Z0-9\!@#$%^\&_.-]* ]]
      then echo $s
      fi
done < "$filename"