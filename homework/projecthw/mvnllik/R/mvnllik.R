#' A package for evaluating log-likelihood function of the multivariate normal.
#' The package contains mvnllik functions.
#'@name mvnllik
#'@title Evaluating Log-likelihood Function of the Multivariate Normal
#'@description
#'This package contains the mvnllik functions for evaluating the log-likelihood functions for 
#' a grid of theta values
#'
#'@docType package
#'
#'@useDynLib mvnllik, .registration=TRUE
#' 
#' @examples 
#' thetas <- seq(0.1, 3, length=10)
#' X <- runif(30, -1, 1)
#' D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
#' Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
#' Y <- mvtnorm::rmvnorm(100, sigma=Sigma)
#' ll.R <- logliks.R(Y, D, thetas, verb=0, ll="loglik")
#' ll.R2 <- logliks.R(Y, D, thetas, verb=0, ll="loglik2")
#' ll.C <- logliks(Y, D, thetas, verb=0)
#' 
NULL
#> NULL