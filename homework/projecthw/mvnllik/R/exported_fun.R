#' logliks.R
#'
#' @description wrapper function around log likelihood evaluation (by loglik or loglik2), iterating over each theta value
#' @param Y input MVN 
#' @param D input matrix for constructing the covariance structure
#' @param thetas paramters for defining the covariance structure
#' @param verb input integer, if verb > 0, the function will print out the number of iterations every "verb" times
#' @param ll the selected method to evaluate log likelihood function, "loglik" or "loglik2" 
#' @return values of log-likelihood for a grid of theta values
#' @export
#'
logliks.R <- function(Y, D, thetas, verb=0, ll="loglik") 
{
  llik <- rep(NA, length(thetas))
  switch(ll,
         "loglik"={
         for(t in 1:length(thetas)) {
            llik[t] <- loglik(Y, D, thetas[t])
            if(verb > 0 &&  t %% verb == 0) cat("t=", t, ", ll=", llik[t], "\n", sep="")
           }
         },
         "loglik2"={
           for(t in 1:length(thetas)) {
             llik[t] <- loglik2(Y, D, thetas[t])
             if(verb > 0 &&  t %% verb == 0) cat("t=", t, ", ll=", llik[t], "\n", sep="")
           }
         })
  return(llik)
}


#' logliks
#'
#' @description R interface to fast C version of logliks.R
#' @param Y input MVN 
#' @param D input matrix for constructing the covariance structure
#' @param thetas paramters for defining the covariance structure
#' @param verb input integer, if verb > 0, the function will print out the number of iterations every "verb" times
#' @return values of log-likelihood for a grid of theta values
#' @export 
#' 
logliks <- function(Y, D, thetas, verb=0) 
  {
    m <- nrow(D)
    if(ncol(Y) != m) stop(" dimension mismatch")

    ret <- .C("logliks_R",
      n = as.integer(nrow(Y)),
      m = as.integer(m),
      Y = as.double(t(Y)),
      D = as.double(D),         ## symmetric, no transpose
      thetas = as.double(thetas),
      tlen = as.integer(length(thetas)),
      verb = as.integer(verb),
      llik = double(length(thetas)),PACKAGE = "mvnllik")

    return(ret$llik)
  }


