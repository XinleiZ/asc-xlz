#include <RcppArmadillo.h>

using namespace Rcpp;
using namespace arma;


// [[Rcpp::depends(RcppArmadillo)]]

// [[Rcpp::export]]
double loglik2_cpp(mat Y, mat D, double theta){
    int m = D.n_cols;
    int n = Y.n_rows;
    if(Y.n_cols != m) stop(" dimension mismatch");
    double SDEPS = sqrt(DBL_EPSILON);
    mat Sigma(m,m);
    for(int i=0; i<m; i++){
      Sigma(i,i) = 1+SDEPS;
      for(int j=i+1; j<m; j++){
        Sigma(i,j) = Sigma(j,i) = exp(0-D(i,j)/theta);
      }
    }
    mat Schol;
    chol(Schol,Sigma);
    double ldet = 2*sum(log(Schol.diag())); 
    mat Si = inv_sympd(Sigma); ;
    double ll = -0.5*n*(m*log(2*M_PI) + ldet);
    for(int i=0; i<n; i++){
      vec temp = Y.row(i)*Si*Y.row(i).t();
      ll -= 0.5*temp(0);
    }
    return(ll);
}


