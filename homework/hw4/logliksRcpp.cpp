#include <Rcpp.h>
#include <iostream>
using namespace Rcpp;

extern "C"{
#include "src/mvnllik.c"
}


// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::export]]
NumericVector logliks_cpp(NumericMatrix Y_in, NumericMatrix D_in, NumericVector theta_in, int verb=0) {
  int m_in = D_in.nrow();
  int n_in = Y_in.ncol();
  int tlen_in = theta_in.length();
  double llik[tlen_in];


  double **Y, **D;
  /* change a vector representation of Y into a array one */
  Y = (double **) malloc(sizeof(double*) * (n_in));
  Y[0] =&Y_in(0,0);
  for(int i=1; i<n_in; i++) Y[i] = Y[i-1] + m_in;
  
  D = (double **) malloc(sizeof(double*) * (m_in));
  D[0] = &D_in(0,0);
  for(int i=1; i<m_in; i++) D[i] = D[i-1] + m_in;
  
  /* call the C-side subroutine */
  logliks(n_in, m_in, Y, D, theta_in.begin(), 
          tlen_in, verb, llik);
  /* clean up */
  free(Y);
  free(D);
  
  NumericVector llik_out(tlen_in);
  for(int i=0; i<tlen_in; i++) llik_out[i] = llik[i];
  return(llik_out);
}




